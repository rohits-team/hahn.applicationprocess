﻿export class ValidationError extends Error {
  public validationErrors: any;

  constructor(message, validationErrors) {
    super(message);
    this.name = "ValidationError";
    this.validationErrors = validationErrors;
  }
}
