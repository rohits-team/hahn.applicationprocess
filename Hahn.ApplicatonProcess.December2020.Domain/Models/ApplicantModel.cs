﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Domain.Models
{
    public class ApplicantModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string FamilyName { get; set; }

        public string Address { get; set; }

        public string Country { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public bool? Hired { get; set; }

        //public DateTime CreatedOnUtc { get; set; }

        //public DateTime UpdatedOnUtc { get; set; }
    }
}
