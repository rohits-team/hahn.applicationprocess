export interface Applicant {
  id: number;
  name: string;
  familyName: string;
  address: string;
  email: string;
  age: number;
  country: string;
  hired: boolean;
}

export const getApplicantDefaults = () => {
  return {
    id: 0,
    name: '',
    familyName: '',
    address: '',
    email: '',
    age: 20,
    country: '',
    hired: false
  };
}
