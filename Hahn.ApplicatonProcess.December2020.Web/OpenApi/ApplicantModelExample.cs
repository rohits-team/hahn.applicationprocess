﻿using Hahn.ApplicatonProcess.December2020.Domain.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Web.OpenApi
{
    public class ApplicantModelExample : IExamplesProvider<Domain.Models.ApplicantModel>
    {
        public ApplicantModel GetExamples()
        {
            return new ApplicantModel
            {
                Name = "Dieter",
                FamilyName = "Schreier",
                Address = "Brandenburgische Strasse 95, Freisbach, Rheinland-Pfalz, 67361",
                Age = 32,
                Country = "Germany",
                Email = "dieter.schreier@example.com",
                Hired = true
            };
        }
    }
}
