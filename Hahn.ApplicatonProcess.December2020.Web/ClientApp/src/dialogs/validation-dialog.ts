﻿import { autoinject } from "aurelia-framework";
import { DialogController } from 'aurelia-dialog';

@autoinject
export class ValidationDialog {

  constructor(public controller: DialogController) {

  }

  validationErrors: Array<any>;
  activate(data) {
    this.validationErrors = data;
  }
}
