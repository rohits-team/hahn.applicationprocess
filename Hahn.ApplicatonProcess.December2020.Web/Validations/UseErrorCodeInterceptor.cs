﻿using FluentValidation;
using FluentValidation.AspNetCore;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Web.Validations
{
    public class UseErrorCodeInterceptor : IValidatorInterceptor
    {
        public IValidationContext BeforeMvcValidation(ControllerContext controllerContext, IValidationContext commonContext)
        {
            return commonContext;
        }

        public ValidationResult AfterMvcValidation(ControllerContext controllerContext, IValidationContext commonContext, ValidationResult result)
        {
            var projection = result.Errors.Select(
                failure => new ValidationFailure(
                    failure.PropertyName,
                    JsonConvert.SerializeObject(new
                    {
                        message = failure.ErrorMessage,
                        placeholderValues = JsonConvert.SerializeObject(failure.FormattedMessagePlaceholderValues),
                        code = failure.ErrorCode
                    })));
            return new ValidationResult(projection);
        }
    }
}
