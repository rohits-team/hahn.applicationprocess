﻿using AutoMapper;
using FluentValidation;
using FluentValidation.AspNetCore;
using Hahn.ApplicatonProcess.December2020.Domain.Services.Interfaces;
using Hahn.ApplicatonProcess.December2020.Web.OpenApi;
using Hahn.ApplicatonProcess.December2020.Web.Validations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;
using System.Net;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApplicantController : ControllerBase
    {
        private readonly ILogger<ApplicantController> _logger;

        private readonly IApplicantService _service;

        private readonly IMapper _mapper;

        private readonly IValidationProvider<Domain.Models.ApplicantModel> _validator;

        public ApplicantController(ILogger<ApplicantController> logger,
            IApplicantService service,
            IMapper mapper,
            IValidationProvider<Domain.Models.ApplicantModel> validator)
        {
            _logger = logger;
            _service = service;
            _mapper = mapper;
            _validator = validator;
        }

        [HttpGet("{id:required:int}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(Domain.Models.ApplicantModel), Description = "Applicant found and returned successfully")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Applicant could not be found")]
        public async Task<IActionResult> Get(int id)
        {
            var applicant = await _service.GetAsync(id);

            if (applicant == null) return NotFound();

            return Ok(applicant);
        }

        [HttpPost]
        [SwaggerRequestExample(typeof(Domain.Models.ApplicantModel), typeof(ApplicantModelExample))]
        [SwaggerResponse((int)HttpStatusCode.Created, Type = typeof(Domain.Models.ApplicantModel), Description = "Applicant created and returned successfully")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Invalid data for Applicant")]
        public async Task<IActionResult> Post(Domain.Models.ApplicantModel applicant)
        {
            var validationResult = await _validator.ValidateAsync(ModelState, applicant);
            if (!validationResult)
            {
                return BadRequest(ModelState);
            }

            var addedModel = await _service.AddAsync(applicant);
            
            return Created(Url.Action("Get", "Applicant", new { id = addedModel.ID }), addedModel);
        }

        [HttpPut]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(Domain.Models.ApplicantModel), Description = "Applicant updated and returned successfully")]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Invalid data for Applicant")]
        public async Task<IActionResult> Put(Domain.Models.ApplicantModel applicant)
        {
            //ASP.NET’s validation pipeline is not asynchronous, so validation using validator directly
            var validationResult = await _validator.ValidateAsync(ModelState, applicant);
            if (!validationResult)
            {
                return BadRequest(ModelState);
            }

            var updatedModel = await _service.UpdateAsync(applicant);

            return Ok(updatedModel);
        }

        [HttpDelete("{id:required:int}")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(Domain.Models.ApplicantModel), Description = "Applicant deleted successfully")]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Applicant could not be found")]
        public async Task<IActionResult> Delete(int id)
        {
            var deletedModel = await _service.DeleteAsync(id);

            if (deletedModel == null) return NotFound();

            _logger.LogInformation($"Attempt to delete applicant with ${id} succeeded.");

            return Ok();
        }


    }
}
