﻿import { inject, bindable, Disposable } from 'aurelia-framework';
import { I18N } from 'aurelia-i18n';
import * as $ from "jquery";
import { map } from 'lodash';
import { EventAggregator } from 'aurelia-event-aggregator';
import 'select2';
import 'select2/dist/js/i18n/de';
import 'select2/dist/js/i18n/en'
import 'select2/dist/css/select2.min.css';
import 'select2-theme-bootstrap4/dist/select2-bootstrap.min.css';

@inject(Element, I18N, EventAggregator)
export class CountryAutoCompleteCustomElement {

  private eventSubscription: Disposable;

  constructor(private element: Element,
    private i18n: I18N,
    private eventAggregator: EventAggregator) {
  }

  attached() {
    this.setup();
    
    this.eventSubscription = this.eventAggregator.subscribe('i18n:locale:changed', payload => {
      jQuery(this.element).select2('destroy');
      this.setup();
    });
  }

  setup(value?: string) {
    jQuery(this.element).select2({
      language: this.i18n.getLocale(),
      minimumInputLength: 3,
      width: '100%',
      theme: 'bootstrap',
      ajax: {
        url: (params) => `https://restcountries.eu/rest/v2/name/${params.term}?fields=name`,
        processResults: function (data) {
          return {
            results: map(data, (e) => { return { id: e.name, text: e.name }; })
          };
        }
      }
    });
  }

  detached() {
    this.eventSubscription.dispose();
  }
}
