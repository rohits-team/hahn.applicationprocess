﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Data.Repositories.Interfaces
{
    public interface IRepository<T> where T: class
    {
        Task<T> GetAsync(int id);

        Task AddAsync(T model);

        void Remove(T model);

        void Update(T model);
    }
}
