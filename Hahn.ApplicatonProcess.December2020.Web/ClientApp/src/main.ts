﻿import 'whatwg-fetch'; // just for preloading fetch polyfill
import { Aurelia } from 'aurelia-framework';
import {I18N, TCustomAttribute} from 'aurelia-i18n';
import {ValidationMessageProvider} from 'aurelia-validation';
import HttpApi from 'i18next-http-backend';
import { EventAggregator } from 'aurelia-event-aggregator';
import { AppRouter } from 'aurelia-router';
// we want font-awesome to load as soon as possible to show the fa-spinner
import '../styles/styles.css';
import 'font-awesome/css/font-awesome.css';

// comment out if you don't want a Promise polyfill (remove also from webpack.config.js)
import * as Bluebird from 'bluebird';
import { parse } from 'ts-node';
Bluebird.config({ warnings: false });

export async function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .plugin("aurelia-dialog")
    .plugin("aurelia-validation") 
    .plugin('aurelia-i18n', (instance) => {
      let aliases = ['t', 'i18n'];
      // add aliases for 't' attribute
      TCustomAttribute.configureAliases(aliases);

      // register backend plugin
      instance.i18next.use(HttpApi);

      // adapt options to your needs (see http://i18next.com/docs/options/)
      // make sure to return the promise of the setup method, in order to guarantee proper loading
      return instance.setup({
        backend: {                                  // <-- configure backend settings
          loadPath: './locales/{{lng}}/{{ns}}.json', // <-- XHR settings for where to get the files from
        },
        whitelist: ['en', 'de'],
        preload: ['en', 'de'],
        ns: 'translation',
        defaultNS: 'translation',
        fallbackNS: false,
        attributes: aliases,
        lng : 'de',
        fallbackLng : 'en',
        debug : false
      }).then(() => {
        const router = aurelia.container.get(AppRouter);
        router.transformTitle = title => instance.tr(title);

        const eventAggregator = aurelia.container.get(EventAggregator);
        eventAggregator.subscribe('i18n:locale:changed', () => {
          router.updateTitle();
        });
      });
    });

    ValidationMessageProvider.prototype.getMessage = function(key) {
      const i18n = aurelia.container.get(I18N);
      const translation = i18n.tr(`errorMessages.${key}`);
      return this.parser.parse(translation);
    };
  
    ValidationMessageProvider.prototype.getDisplayName = function(propertyName: string, displayName: string): string {
      if (displayName !== null && displayName !== undefined) {
        return displayName;
      }
      const i18n = aurelia.container.get(I18N);
      return i18n.tr("labels." + propertyName);
    };

  // Uncomment the line below to enable animation.
  // aurelia.use.plugin('aurelia-animator-css');
  // if the css animator is enabled, add swap-order="after" to all router-view elements

  // Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  // aurelia.use.plugin('aurelia-html-import-template-loader')

  await aurelia.start();
  aurelia.setRoot('modules/app/app');

  // if you would like your website to work offline (Service Worker), 
  // install and enable the @easy-webpack/config-offline package in webpack.config.js and uncomment the following code:
  /*
  const offline = await System.import('offline-plugin/runtime');
  offline.install();
  */
}
