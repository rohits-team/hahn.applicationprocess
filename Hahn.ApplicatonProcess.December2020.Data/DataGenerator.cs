﻿using Hahn.ApplicatonProcess.December2020.Data.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Data
{
    public class DataGenerator
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new EfDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<EfDbContext>>()))
            {
                context.Database.EnsureCreated();

                if (context.Applicants.Any())
                {
                    return; // Data was already seeded
                }

                context.Applicants.AddRange(
                    new Models.Applicant
                    {
                        ID = 1,
                        Name = "Amalie",
                        FamilyName = "Ritter",
                        Address = "Buelowstrasse 47, Kölbingen, Rheinland-Pfalz, 56459",
                        Age = 36,
                        CountryOfOrigin = "Germany",
                        EmailAdress = "amalie.ritter@example.com",
                        Hired = true
                    },
                    new Models.Applicant
                    {
                        ID = 2,
                        Name = "Herbert",
                        FamilyName = "Stumpf",
                        Address = "Schäfers Gärten 14, Frankfurt Am Main, 60431",
                        Age = 42,
                        CountryOfOrigin = "Germany",
                        EmailAdress = "herbert.stumpf@example.com",
                        Hired = true
                    },
                    new Models.Applicant
                    {
                        ID = 3,
                        Name = "Ruperta",
                        FamilyName = "Taube",
                        Address = "Fugger Strasse 85, Koblenz Kesselheim, Rheinland-Pfalz, 56070",
                        Age = 42,
                        CountryOfOrigin = "Germany",
                        EmailAdress = "ruperta.taube@example.com",
                        Hired = true
                    });

                context.SaveChanges();
            }
        }
    }
}
