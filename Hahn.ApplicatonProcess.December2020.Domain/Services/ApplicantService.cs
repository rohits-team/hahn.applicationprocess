﻿using AutoMapper;
using Hahn.ApplicatonProcess.December2020.Data.Repositories.Interfaces;
using Hahn.ApplicatonProcess.December2020.Domain.Options;
using Hahn.ApplicatonProcess.December2020.Domain.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Domain.Services
{
    internal class ApplicantService : IApplicantService, IDisposable
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly ILogger<ApplicantService> _logger;

        private readonly Lazy<IOptions<ExternalServicesOptionsModel>> _externalServicesOptions;

        private readonly IMapper _mapper;

        private readonly IHttpClientFactory _clientFactory;

        public ApplicantService(IUnitOfWork unitOfWork,
            ILogger<ApplicantService> logger,
            IMapper mapper,
            Lazy<IOptions<ExternalServicesOptionsModel>> externalServicesOptions,
            IHttpClientFactory clientFactory)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _mapper = mapper;
            _externalServicesOptions = externalServicesOptions;
            _clientFactory = clientFactory;
        }

        public async Task<Domain.Models.ApplicantModel> AddAsync(Domain.Models.ApplicantModel applicant)
        {
            //prevent adding duplicate PK
            applicant.ID = 0;
            var dbApplicant = _mapper.Map<Data.Models.Applicant>(applicant);
            await this._unitOfWork.Applicants.AddAsync(dbApplicant);
            await this._unitOfWork.SaveChangesAsync();
            return _mapper.Map<Domain.Models.ApplicantModel>(dbApplicant);
        }

        public async Task<Domain.Models.ApplicantModel> DeleteAsync(int id)
        {
            var dbApplicant = await this._unitOfWork.Applicants.GetAsync(id);
            this._unitOfWork.Applicants.Remove(dbApplicant);
            await this._unitOfWork.SaveChangesAsync();
            return _mapper.Map<Domain.Models.ApplicantModel>(dbApplicant);
        }

        public void Dispose()
        {
            this._unitOfWork.Dispose();
        }

        public async Task<Domain.Models.ApplicantModel> GetAsync(int id)
        {
            var dbApplicant = await this._unitOfWork.Applicants.GetAsync(id);
            return _mapper.Map<Domain.Models.ApplicantModel>(dbApplicant);
        }

        public async Task<Domain.Models.ApplicantModel> UpdateAsync(Domain.Models.ApplicantModel applicant)
        {
            var dbApplicant = _mapper.Map<Data.Models.Applicant>(applicant);
            this._unitOfWork.Applicants.Update(dbApplicant);
            await this._unitOfWork.SaveChangesAsync();
            return _mapper.Map<Domain.Models.ApplicantModel>(dbApplicant);
        }

        public async Task<bool> ValidateApplicantCountry(string country, CancellationToken cancellationToken)
        {
            var externalServicesOptions = _externalServicesOptions.Value;
            var url = string.Format(externalServicesOptions.Value.CountryNameValidationApi, country);
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            var stopwatch = new Stopwatch();

            _logger.LogInformation("Initiating request GET: {0}", url);

            using (var client = _clientFactory.CreateClient())
            {
                stopwatch.Start();
                var response = await client.SendAsync(request, cancellationToken);
                stopwatch.Stop();

                _logger.LogInformation("Request to GET: {0} completed with status: {1} and took: {2} ms", url, response.StatusCode, stopwatch.ElapsedMilliseconds);
                if (!response.IsSuccessStatusCode)
                {
                    _logger.LogError("Request to country api GET: {0}, failed with status: {1}", url, response.StatusCode);
                    return false;
                }

                return true;
            }
        }
    }
}
