﻿using Hahn.ApplicatonProcess.December2020.Data.Contexts;
using Hahn.ApplicatonProcess.December2020.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EfDbContext _database;

        public UnitOfWork(EfDbContext context)
        {
            this._database = context;
            this.Applicants = new ApplicantRepository(context);
        }

        public IApplicantRepository Applicants { get; }

        public void Dispose()
        {
            _database.Dispose();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await this._database.SaveChangesAsync();
        }
    }
}
