# Hahn Appplication Project

This is a SPA and web api project for applicants to provide their deatils and submit job applications. It's built with following stack:

 - [Aurelia](https://aurelia.io/) / Typescript / Webpack for frontend.
 - .NET 5 / ASP.NET Core for backend.
 - [EntityFrameworkCore 5.0](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore/5.0.1) In-Memory store for temporary data store.

## Structure

 - *Hahn.ApplicatonProcess.December2020.Data* - Data access project.
 - *Hahn.ApplicatonProcess.December2020.Domain* - Business Logic and domain
 - *Hahn.ApplicatonProcess.December2020.Web* - Web API project
 - *Hahn.ApplicatonProcess.December2020.Web/ClientApp* - Aurelia frontend SPA application

## Tools

 - Visual Studio 2019 16.8 community edition
 - .NET 5.0
 - skeleton-typescript-webpack from Aurelia [skeleton-navigation](https://github.com/aurelia/skeleton-navigation) as base project
 - NodeJS 14.15.4

## Internationalization (i18n)

The frontend application has i18n support and currently has English (EN) and German (DE) lanuages supported.

The resources for translations are in *Hahn.ApplicatonProcess.Application\Hahn.ApplicatonProcess.December2020.Web\ClientApp\src\locales* directory

## Build and run

Install the npm dependencies for Aurelia frontend (ClientApp) application, by running ```npm install``` on the ClientApp directory.

After installing the dependencies for the frontend application, you need to build and run the Aurelia frontend (ClientApp) project with ```npm start``` command. This will start dev server listening on localhost, port 9000.

Next, run the *Hahn.ApplicatonProcess.December2020.Web* web api project, using Visual Studio. This should open the Swagger UI for the API

The access to frontend application is proxied through web api project using ```UseProxyToSpaDevelopmentServer``` method on AspNetCore project, so you MUST access the frontend application using the web api project and not through dev server directly (i.e. not on localhost, port 9000). The frontend application can be accessed from root url of the web api project.

The aurelia webpack hot reload server seems to be having issues with Firefox and works well with Chrome browser.

## TODO improvements

 - Errorcode in validation errors from web api POST/PUT. This helps is keeping complete responsibility of translation on client-side, which can do so using error codes.
