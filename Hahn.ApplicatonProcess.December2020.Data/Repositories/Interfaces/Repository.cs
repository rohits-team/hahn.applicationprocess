﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Data.Repositories.Interfaces
{
    internal abstract class Repository<T> : IRepository<T> where T: class
    {
        private readonly DbSet<T> entities;

        public Repository(DbContext dbContext)
        {
            this.entities = dbContext.Set<T>();
        }

        public async Task<T> GetAsync(int id)
        {
            return await this.entities.FindAsync(id);
        }

        public async Task AddAsync(T model)
        {
            await this.entities.AddAsync(model);
        }

        public void Remove(T model)
        {
            this.entities.Remove(model);
        }

        public void Update(T model)
        {
            this.entities.Update(model);
        }
    }
}
