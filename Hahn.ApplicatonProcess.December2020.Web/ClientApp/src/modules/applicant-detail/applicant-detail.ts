import { lazy, autoinject} from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import { Applicant, getApplicantDefaults } from './../../models/applicant';
import { I18N } from 'aurelia-i18n';

@autoinject
export class Users {
  heading: string = 'Applicant details';
  applicant: Applicant = getApplicantDefaults();
  http: HttpClient;
  showSuccessMessage: boolean = false;

  constructor(@lazy(HttpClient) private getHttpClient: () => HttpClient,
    private i18n: I18N) { }

  async activate(params): Promise<void> {

    const http = this.http = this.getHttpClient();

    const response = await http.fetch('applicant/' + params.id);
    this.applicant = await response.json();
    this.showSuccessMessage = params.status && params.status === 'created';
  }
}

export class StringNullValueConverter {
  toView(value: string): string {
    return value ? value : "-";
  }
}
