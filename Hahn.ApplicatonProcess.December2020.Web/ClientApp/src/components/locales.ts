import {I18N} from 'aurelia-i18n';
import { inject } from 'aurelia-framework';

interface Locale {
  title: string,
  code: string,
  flag: string
}

@inject(I18N)
export class Locales {
  private locales: Array<Locale>;
  private currentLocale: string;

  constructor(private i18n: I18N) {
    this.locales = [
      {
        title: "English",
        code: "en",
        flag: "england"
      },
      {
        title: "German",
        code: "de",
        flag: "germany"
      }
    ]
    this.currentLocale = this.i18n.getLocale();
  }

  setLocale(locale) {
    let code = locale.code
    if(this.currentLocale !== code) {
      this.i18n.setLocale(code);
      this.currentLocale = code;
    }
  }
}
