import {Router, RouterConfiguration} from 'aurelia-router';

export class ChildRouter {
  router: Router;

  heading = 'Child Router';

  configureRouter(config: RouterConfiguration, router: Router) {
    config.map([
      { route: ['', 'welcome'], name: 'welcome',       moduleId: './../modules/welcome/welcome',       nav: true, title: 'Welcome' },
      { route: 'applicant/:id/detail/:status?', name: 'applicant-detail', moduleId: './../modules/applicant-detail/applicant-detail', nav: true, title: 'Applicant details' }
    ]);

    this.router = router;
  }
}
