﻿using Hahn.ApplicatonProcess.December2020.Data.Contexts;
using Hahn.ApplicatonProcess.December2020.Data.Models;
using Hahn.ApplicatonProcess.December2020.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Data.Repositories
{
    internal class ApplicantRepository : Repository<Applicant>, IApplicantRepository
    {
        public ApplicantRepository(EfDbContext context) : base(context)
        { }
    }
}
