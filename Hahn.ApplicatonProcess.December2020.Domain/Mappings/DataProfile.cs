﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Domain.Mappings
{
    public class DataProfile : Profile
    {
        public DataProfile()
        {
            CreateMap<Data.Models.Applicant, Models.ApplicantModel>()
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.CountryOfOrigin))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.EmailAdress))
                .ReverseMap();
        }
    }
}
