﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Domain.Services.Interfaces
{
    public interface IApplicantService
    {
        Task<Domain.Models.ApplicantModel> AddAsync(Domain.Models.ApplicantModel applicant);

        Task<Domain.Models.ApplicantModel> DeleteAsync(int id);

        Task<Domain.Models.ApplicantModel> GetAsync(int id);

        Task<Domain.Models.ApplicantModel> UpdateAsync(Domain.Models.ApplicantModel applicant);

        Task<bool> ValidateApplicantCountry(string country, CancellationToken cancellationToken);
    }
}
