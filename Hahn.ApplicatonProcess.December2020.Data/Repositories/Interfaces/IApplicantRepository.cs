﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Data.Repositories.Interfaces
{
    public interface IApplicantRepository : IRepository<Models.Applicant>
    {
        
    }
}
