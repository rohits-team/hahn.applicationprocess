import {computedFrom} from 'aurelia-framework';
import { Disposable, inject, NewInstance } from "aurelia-framework";
import { DialogService } from "aurelia-dialog";
import { I18N } from 'aurelia-i18n';
import { Router } from 'aurelia-router';
import { ValidationRules, Validator, ValidationController, validateTrigger } from 'aurelia-validation';
import { map, keys, isEqual, camelCase } from 'lodash';
import { ValidationError } from './../../common/custom-errors';
import { Confirm } from "./../../dialogs/confirm";
import { ValidationDialog } from "./../../dialogs/validation-dialog";
import { CountryAutoCompleteCustomElement } from './../../components/country-auto-complete';
import { ApplicantService } from './../../services/applicant-service';
import { Applicant, getApplicantDefaults } from "./../../models/applicant";
import { BootstrapFormRenderer } from './../../common/bootstrap-form-renderer';

@inject(DialogService, I18N, Validator, NewInstance.of(ValidationController), NewInstance.of(ApplicantService), Router) 
export class Welcome {
  heading: string = 'Welcome to the Hahn Application Process';
  applicant: Applicant = getApplicantDefaults();
  emptyApplicant: Applicant = getApplicantDefaults();
  submitInProgress: boolean = false;
  errorMessage: string = '';
  successMessage: string = '';
  preventSave: boolean = false;
  private validationSubscription: Disposable;

  constructor(private dailogService: DialogService, 
      private i18n: I18N, 
      private validator: Validator,
      private controller: ValidationController,
      private applicantService: ApplicantService,
      private router: Router) {
    this.controller.validateTrigger = validateTrigger.changeOrBlur;
    this.controller.addRenderer(new BootstrapFormRenderer());
    this.configureValidationRules();
  }

  @computedFrom('applicant.name', 'applicant.familyName', 'applicant.address', 'applicant.email', 'applicant.age', 'applicant.country', 'applicant.hired')
  get isFormDirty(): boolean {
    return !isEqual(this.applicant, this.emptyApplicant);
  }

  attached() {
    this.validationSubscription = this.controller.subscribe((event) => this.validateForm());
  }

  detached() {
    this.validationSubscription.dispose();
  }

  private validateForm() {
    this.validator.validateObject(this.applicant)
      .then(results => this.preventSave = !results.every(result => result.valid));
  }

  private configureValidationRules() {
    ValidationRules
      .ensure('name').required().minLength(5)
      .ensure('familyName').required().minLength(5)
      .ensure('email').email()
      .ensure('address').minLength(10).when(address => address !== null && address !== undefined && address !== '')
      .ensure('age').required().between(19, 61)
      .ensure('country').required()
      .on(this.applicant);
  }

  confirmReset() {
    var openResult = this.dailogService.open({
        viewModel: Confirm
      , model: this.i18n.tr('messages.resetConfirmation')
    })
    
    openResult.whenClosed(result => {
      if(!result.wasCancelled) {
        this.reset();
      }
    });
  }

  reset() {
    this.applicant = getApplicantDefaults();
    this.controller.reset();
  }

  submit() {
    this.submitInProgress = true;
    this.applicantService.submit(this.applicant)
      .then(data => {
        this.successMessage = this.i18n.tr("application.submitted");
        this.router.navigateToRoute('applicant-detail', { id: data.id, status: 'created' });
      })
      .catch((error: Error): void => {
        if (error.name === ValidationError.name) {
          var validationErrors = (error as ValidationError).validationErrors;
          this.dailogService.open({
            viewModel: ValidationDialog
            , model: map(keys(validationErrors), (ele) => { return { propertyName: this.i18n.tr("labels." + camelCase(ele)), errors: validationErrors[ele] } })
          });
        } else {
          this.errorMessage = this.i18n.tr("messages.errorOccured");
        }
        this.submitInProgress = false;
      });
  }
}

export class UpperValueConverter {
  toView(value: string): string {
    return value && value.toUpperCase();
  }
}
