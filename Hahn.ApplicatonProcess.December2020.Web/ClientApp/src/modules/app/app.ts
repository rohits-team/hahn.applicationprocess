import {Aurelia} from 'aurelia-framework';
import { Locales } from  './../../components/locales';
import {Router, RouterConfiguration} from 'aurelia-router';

export class App {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Hahn Application Process';
    config.map([
      { route: ['', 'welcome'], name: 'welcome', moduleId: './../welcome/welcome', nav: true, title: 'titles.welcome' },
      { route: 'applicant/:id/detail/:status?', name: 'applicant-detail', moduleId: './../applicant-detail/applicant-detail', title: 'titles.applicantDetails' }
    ]);

    this.router = router;
  }
}
