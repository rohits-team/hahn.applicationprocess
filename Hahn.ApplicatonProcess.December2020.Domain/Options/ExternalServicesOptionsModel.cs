﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Domain.Options
{
    public class ExternalServicesOptionsModel
    {
        public const string ExternalServices = "ExternalServices";

        public string CountryNameValidationApi { get; set; }
    }
}
