﻿using Autofac;
using FluentValidation;
using Hahn.ApplicatonProcess.December2020.Domain.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Domain.IocModules
{
    public class ValidationModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ApplicantValidator>().As<IValidator<Models.ApplicantModel>>();

            base.Load(builder);
        }
    }
}
