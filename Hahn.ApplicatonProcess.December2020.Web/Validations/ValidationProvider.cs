﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Web.Validations
{
    public class ValidationProvider<T> : IValidationProvider<T>
    {
        private readonly IValidator<T> _validator;

        public ValidationProvider(IValidator<T> validator)
        {
            this._validator = validator;
        }

        public async Task<bool> ValidateAsync(ModelStateDictionary modelState, T model)
        {
            var validationResult = await _validator.ValidateAsync(model);
            if (!validationResult.IsValid)
            {
                validationResult.AddToModelState(modelState, null);
            }

            return validationResult.IsValid;
        }
    }
}
