﻿using FluentValidation;
using Hahn.ApplicatonProcess.December2020.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Domain.Validations
{
    public class ApplicantValidator : AbstractValidator<Models.ApplicantModel>
    {
        private readonly IApplicantService _applicantService;

        public ApplicantValidator(IApplicantService applicantService)
        {
            this._applicantService = applicantService;

            RuleFor(x => x.Name).NotNull().NotEmpty().MinimumLength(5);
            RuleFor(x => x.FamilyName).NotNull().NotEmpty().MinimumLength(5);
            RuleFor(x => x.Address).MinimumLength(10).When(x => !string.IsNullOrWhiteSpace(x.Address));
            RuleFor(x => x.Email).EmailAddress().WithErrorCode("EmailValidator");
            RuleFor(x => x.Age).InclusiveBetween(20, 60);
            RuleFor(x => x.Hired).NotNull();
            RuleFor(x => x.Country).NotNull().NotEmpty().MustAsync(ValidateCountry).WithMessage("Country must be a valid country").WithErrorCode("CountryValidator");
        }

        private async Task<bool> ValidateCountry(Models.ApplicantModel model, string country, CancellationToken cancellationToken)
        {
            return await _applicantService.ValidateApplicantCountry(country, cancellationToken);
        }
    }
}
