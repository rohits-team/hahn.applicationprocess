﻿import { Applicant } from './../models/applicant';
import { ValidationError } from './../common/custom-errors';
import { lazy } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';

export class ApplicantService {
  http: HttpClient;

  constructor(@lazy(HttpClient) private getHttpClient: () => HttpClient) { }

  async submit(applicant: Applicant): Promise<Applicant> {

    const http = this.http = this.getHttpClient();

    const response = await http.fetch('applicant', {
      method: 'post',
      body: json(applicant)
    });

    if (!response.ok && !(response.status === 201)) {
      const message = `An error has occured: ${response.status}`;
      if (response.status === 400) {
        const errorBody = await response.json();
        throw new ValidationError(message, errorBody.errors);
      } else {
        throw new Error(message);
      }
      
    }

    return await response.json();
  }

}
