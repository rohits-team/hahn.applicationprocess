﻿using AutoMapper;
using Hahn.ApplicatonProcess.December2020.Data;
using Hahn.ApplicatonProcess.December2020.Data.Contexts;
using Hahn.ApplicatonProcess.December2020.Domain.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Domain.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RegisterDbContext(this IServiceCollection services, string databaseName)
        {
            services.AddDbContext<EfDbContext>(options =>
                options.UseInMemoryDatabase(databaseName: databaseName));
        }

        public static void RunSeed(this IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<EfDbContext>();

            //4. Call the DataGenerator to create sample data
            DataGenerator.Initialize(serviceProvider);
        }

        public static void RegisterMappingProfiles(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(DataProfile));
        }
    }
}
