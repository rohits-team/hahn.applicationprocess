﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hahn.ApplicatonProcess.December2020.Web.Validations
{
    public interface IValidationProvider<T>
    {
        Task<bool> ValidateAsync(ModelStateDictionary modelState, T model);
    }
}
