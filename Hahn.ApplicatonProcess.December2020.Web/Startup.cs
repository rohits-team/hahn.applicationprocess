using Autofac;
using AutoMapper;
using FluentValidation.AspNetCore;
using Hahn.ApplicatonProcess.December2020.Domain.Extensions;
using Hahn.ApplicatonProcess.December2020.Domain.IocModules;
using Hahn.ApplicatonProcess.December2020.Domain.Mappings;
using Hahn.ApplicatonProcess.December2020.Domain.Options;
using Hahn.ApplicatonProcess.December2020.Web.IocModules;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System.Net.Mime;

namespace Hahn.ApplicatonProcess.December2020.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.AddHttpClient();

            services.AddControllers(options =>
            {
                options.Filters.Add(new Microsoft.AspNetCore.Mvc.ProducesAttribute(MediaTypeNames.Application.Json));
                options.Filters.Add(new Microsoft.AspNetCore.Mvc.ConsumesAttribute(MediaTypeNames.Application.Json));
            }).AddFluentValidation();

            services.Configure<ExternalServicesOptionsModel>(Configuration.GetSection(ExternalServicesOptionsModel.ExternalServices));

            services.RegisterMappingProfiles();

            services.RegisterDbContext(Configuration["Database"]);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Hahn Applicaton API", Version = "v1" });
                c.ExampleFilters();
            });
            services.AddSwaggerExamplesFromAssemblyOf<OpenApi.ApplicantModelExample>();

            // In production, the Aurelia files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new DataModule());
            builder.RegisterModule(new ServiceModule());
            builder.RegisterModule(new ValidationModule());
            builder.RegisterModule(new WebModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Hahn.ApplicatonProcess.December2020.Web v1"));
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseProxyToSpaDevelopmentServer(Configuration["SpaDevelopmentServerHost"]);
                }
            });
        }
    }
}
