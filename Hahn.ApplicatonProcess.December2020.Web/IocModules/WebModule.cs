﻿using Autofac;
using FluentValidation.AspNetCore;
using Hahn.ApplicatonProcess.December2020.Web.Validations;

namespace Hahn.ApplicatonProcess.December2020.Web.IocModules
{
    public class WebModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<UseErrorCodeInterceptor>().As<IValidatorInterceptor>();

            builder.RegisterGeneric(typeof(ValidationProvider<>))
                .As(typeof(IValidationProvider<>));

            base.Load(builder);
        }
    }
}
